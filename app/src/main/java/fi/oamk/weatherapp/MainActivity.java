package fi.oamk.weatherapp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    // Variable for api key.
    static final String API_KEY = "ec4d9c0f0a9b1dbe1bcd36a32a040bd1";

    // UI views as member variables so they are accessible everywhere inside this class.
    private EditText edLocation;
    private TextView tvTemp;
    private TextView tvWind;
    private TextView tvHumidity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Assign member variables containing UI views.
        edLocation = (EditText)findViewById(R.id.edLocation);
        tvTemp = (TextView)findViewById(R.id.tvTemp);
        tvHumidity = (TextView)findViewById(R.id.tvHumidity);
        tvWind = (TextView)findViewById(R.id.tvWind);
    }

    // Handler for button
    public void getWeather(View view) {
        String city = "";

        // Read city, reset UI and retrieve weather data using AsyncTask, which is executed on it own thread om background.
        city = edLocation.getText().toString();
        String url = "https://api.openweathermap.org/data/2.5/find?q=" + city + "&units=metric&appid=" + API_KEY;
        tvTemp.setText("");
        tvHumidity.setText("");
        tvWind.setText("");
        GetWeatherAsyncTask task = new GetWeatherAsyncTask();
        task.execute(url);
    }


    private class GetWeatherAsyncTask extends AsyncTask<String,String,String> {

        static final int CONNECTION_TIMEOUT = 50000;

        private String error_message = "";

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection urlConnection = null;
            this.error_message = ""; // Reset error.
            try {
                // Open connection and retrieve data.
                URL url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(CONNECTION_TIMEOUT);

                // Get response code and verify that it is 200 ok.
                int responseCode = urlConnection.getResponseCode();
                if (responseCode == 200) {
                    // Get content.
                    String content = streamToString(urlConnection.getInputStream());
                    publishProgress(content); // This will cause onProgressUpdate to be called.
                }
                else {
                    this.error_message = getString(R.string.http_response_error) + " " +  responseCode;
                }

            }
            catch (Exception ex) {
                this.error_message = ex.getMessage().toString();
            }
            finally {
                // Close connection.
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return " ";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Show toast, if there is an error after data retrieval is finished.
            if (this.error_message.length() > 0) {
                Toast toast = Toast.makeText(MainActivity.this, this.error_message, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.LEFT, 0, 0);
                toast.show();
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            // Read JSON data.
            try {
                JSONObject json = new JSONObject(values[0]);
                int count = Integer.parseInt(json.get("count").toString());
                if (count > 0) {
                    JSONArray list = json.getJSONArray("list");
                    JSONObject main = list.getJSONObject(0).getJSONObject("main");
                    String temp = main.get("temp").toString();
                    String humidity = main.get("humidity").toString();
                    JSONObject wind = list.getJSONObject(0).getJSONObject("wind");
                    String windSpeed = wind.get("speed").toString();
                    String degrees = wind.get("deg").toString();

                    tvTemp.setText(temp + " \u2103");
                    tvHumidity.setText(humidity + " %");
                    tvWind.setText(windSpeed +  "m/s (" + degrees + "\u00B0" + ")");
                }
                else {
                    this.error_message = getString(R.string.no_weather_data);
                }
            }
            catch (Exception ex) {
                this.error_message = ex.getMessage().toString();
            }
            finally {

            }
        }

        // Converts stream retrieved through HTTP to string.
        private String streamToString(InputStream in) {
            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();

            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line;

                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            finally {
                if (reader != null) {
                    try {
                        reader.close();
                    }
                    catch (Exception ex) {
                        this.error_message = ex.getMessage().toString();
                    }
                }
            }
            return response.toString();
        }
    }

}
